//
//  ViewController.swift
//  TimesTableSlider
//
//  Created by siid on 12/5/16.
//  Copyright © 2016 Sudip. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var myTable: UITableView!
    let protoCellIdentifer = "protoCell"
    
    @IBOutlet weak var slider: UISlider!
    
    @IBAction func numberSlider(_ sender: UISlider) {
        myTable.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: protoCellIdentifer)
        let selectedNumber = Int(slider.value*20)
        
        
//        cell.textLabel?.text=String(slider.value)
        cell.textLabel?.textAlignment = NSTextAlignment.center
        cell.textLabel?.text = "\(selectedNumber) x \(indexPath.row+1) = \(selectedNumber*(indexPath.row+1))"
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 50
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

